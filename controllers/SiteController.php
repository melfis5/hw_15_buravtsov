<?php
/**
 * Created by PhpStorm.
 * User: melfis
 * Date: 20.02.19
 * Time: 21:00
 */

class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->view->render('sites/index');
    }

    public function actionContacts()
    {
        $this->view->render('sites/contacts');

    }
}