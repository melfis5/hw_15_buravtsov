<?php

require_once '../models/Post.php';

class BlogController extends Controller
{

    public function actionIndex()
    {
        $posts = Post::findAll();

        $this->view->render('blog/index', [
            'pageTitle' => 'Posts lists',
            'posts' => $posts,
        ]);
    }

    public function actionView($id)
    {
        $this->view->render('blog/view', [
            'post' => Post::findOne((int) $id),
        ]);
    }

}