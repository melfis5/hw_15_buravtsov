<?php

require_once '../models/Article.php';

class ArticleController extends Controller
{
    public function actionIndex()
    {

        $this->view->render('article/index', [
            'pageTitle' => 'Articles lists',
            'articles' => Article::findAll(),
        ]);
    }

    public function actionView($id)
    {
        $this->view->render('article/view', [
            'article' => Article::findOne((int) $id),
        ]);
    }

}