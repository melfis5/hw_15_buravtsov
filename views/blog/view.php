<div class="container">
    <h2 align="center"><?= $post['title'] ?></h2>
</div>

<div class="container">
    <p class="h5">
        <?= $post['description'] ?>
    </p>
</div>

<div class="container">
    <a href="/blog">Back</a>
</div>