<div class="container">
    <h2 align="center"><?= $pageTitle ?></h2>
</div>

<div class="container">
    <ul class="list-group">
        <?php foreach ($posts as $key => $post) : ?>
            <li class="list-group-item list-group-item-dark text-center">
                <a href="/blog/view/<?=$key?>"><?= $post['title'] ?></a>
            </li>
        <?php endforeach ?>
    </ul>
</div>