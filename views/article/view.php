<div class="container">
    <h2 align="center"><?= $article['title'] ?></h2>
</div>

<div class="container">
    <p class="h5">
        <?= $article['description'] ?>
    </p>
</div>

<div class="container">
    <a href="/article">Back</a>
</div>