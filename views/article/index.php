<div class="container">
    <h2 align="center"><?= $pageTitle ?></h2>
</div>

<ul class="list-group container">
    <?php foreach ($articles as $key => $article) : ?>
        <li class="list-group-item list-group-item-dark text-center">
            <a href="/article/view/<?=$key?>"><?= $article['title'] ?></a>
        </li>
    <?php endforeach ?>
</ul>