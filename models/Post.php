<?php
/**
 * Created by PhpStorm.
 * User: melfis
 * Date: 24.02.19
 * Time: 17:30
 */

class Post extends Model
{
    public static function findAll()
    {
        return Model::getAll('posts');
    }

    public static function findOne($id)
    {
        $post = self::findAll();

        if (!isset($post[$id])) {
            throw new Exception('No item found');
        }

        return $post[$id];
    }
}