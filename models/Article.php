<?php
/**
 * Created by PhpStorm.
 * User: melfis
 * Date: 27.02.19
 * Time: 15:39
 */

class Article extends Model
{
    public static function findAll()
    {
        return Model::getAll('articles');
    }

    public static function findOne($id)
    {
        $post = self::findAll();

        if (!isset($post[$id])) {
            throw new Exception('No item found');
        }

        return $post[$id];
    }
}