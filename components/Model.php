<?php

define('DB_DRIVER','mysql');
define('DB_HOST','localhost');
define('DB_NAME','aaa');
define('DB_USER','root');
define('DB_PASS','root');

class Model
{
    public static $db;

    public function __construct()
    {
        try {
            $connect_str = DB_DRIVER . ':host='. DB_HOST . ';dbname=' . DB_NAME;
            self::$db = new PDO($connect_str, DB_USER, DB_PASS);

        } catch (PDOException $e) {
            echo 'Database connection error';
            die;
        }
    }

    public static function getAll($tableName)
    {
        $sql = 'SELECT * FROM '.$tableName;
        $query = Self::$db->query($sql);
        return $query->fetchall(PDO::FETCH_ASSOC);
    }
}