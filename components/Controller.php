<?php
/**
 * Created by PhpStorm.
 * User: melfis
 * Date: 20.02.19
 * Time: 20:57
 */

abstract class Controller
{
    public $view;
    public $model;

    public function __construct()
    {
        $this->view = new view();
        $this->model = new Model();
    }

    abstract public function actionIndex();
}