<?php

require_once 'Model.php';

try {
    $db = new Model();
    $db = Model::$db;


    $sql = '
        CREATE TABLE posts(
            id int not NULL auto_increment primary key,
            title varchar(255),
            short varchar(255),
            description text
        );
    ';

    $db->exec($sql);

    $sql = '
        CREATE TABLE articles(
            id int not NULL auto_increment primary key,
            title varchar(255),
            description text
        );
    ';

    $db->exec($sql);

    for ($i = 1; $i <= 3; $i++) {
        $sql = '
                INSERT INTO posts SET
                title = "Name title",
                short = "short description",
                description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cumque dolore inventore labore, perspiciatis quas. A, aliquam amet doloremque explicabo mollitia saepe voluptate. Blanditiis consectetur cumque dolore repudiandae similique tempore?"
            ';

        $db->exec($sql);

        $sql = '
                INSERT INTO articles SET
                title = "name Article",
                description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi cumque dolore inventore labore, perspiciatis quas. A, aliquam amet doloremque explicabo mollitia saepe voluptate. Blanditiis consectetur cumque dolore repudiandae similique tempore?"
            ';
        $db->exec($sql);
    }
    echo 'clean';

} catch (PDOException $e) {
    echo 'PDO error';
}